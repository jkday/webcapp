# WebCapp
## What is WebCapp?
The purpose of the __WebCapp__ is a simple C web framework application, https://facil.io, with C code for live customer demonstration purposes. It is easy to deploy in either: Standalone, Docker or Kubernetes environments with ease. The code will get a random number (CER - Deterministic random number generation API used) and then run a function to determine if the number is abundant (https://www.geeksforgeeks.org/abundant-number/). If it is abundant, the code will also make an outbound http connection to an address located in Malaysia, http://202.165.23.45/, which will trigger when using a custom alert to notify on outbound connections to Malaysia. 

It is meant to highlight the following items during a customer demo:All 4 elements of the Security Risks:

* Code Execution Risks

* AppSec Policy Violation

* OWASP Alerts

* Vulnerable Dependancies

Because this single application is written in C, showing source code stack trace data is possible and ZAP scans run against the web UI quickly to generate core data.


## USAGE

The application is in an “OLD” Ubuntu18.04 docker container missing updates to trigger vulnerability and dependency issues. The default entrypoint is to run the WebCapp from `/home/user/webcapp/tmp/fioapp` in the container. Remember to allow port 3000 access for web access and ZAP scanning.  
`docker run -ti --rm -p 3000:3000 johnkday/fioapp:2.0`  

You can also build your own container by pulling the code from:  
https://gitlab.com/jkday/webcapp  
and build using  
`docker build -t <container image name> .`  
in the git directory.

