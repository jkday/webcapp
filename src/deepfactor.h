
#include <stdbool.h>
// Functions used in deepfactor addon

int getSum(int n);

bool checkAbundant(int n);

int getPage(void);