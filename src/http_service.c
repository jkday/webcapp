#include "fio_cli.h"
#include "main.h"
#include "deepfactor.h"

const char *html_header = "<!DOCTYPE html>\
<html lang=\"en\">\
<head>\
    <meta charset=\"UTF-8\">\
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\
    <title>Math Abundant</title>\
</head>\
<style>\
h1 {text-align: center;}\
</style>\
<style>\
h2 {text-align: center;}\
</style>\
<body>\
";

const char *html_footer = "\
<br><br><h2 style=\"color:Black\">An abundant number is a number that is smaller than the sum of its proper divisors.\n</h1>\
</body>\
</html>";

static void on_http_request(http_s *h)
{

  /* set a response and send it (finnish vs. destroy). */
  int randomNum = rand() % 32768;

  char bodyText[100];
  int bodyLength;

  char httpResponse[1000];
  int httpResponseLength;

  if (checkAbundant(randomNum))
  {
    bodyLength = sprintf(bodyText, "<h1 style=\"color:blue\">%d is Abundant!\n</h1>", randomNum);
    getPage();
  }
  else
  {
    bodyLength = sprintf(bodyText, "<h1 style=\"color:red\">%d is <b>NOT</b> Abundant!\n</h1>", randomNum);
  }
  // Copy in the HTML header
  strcpy(httpResponse, html_header);
  // Add in the body text
  strcat(httpResponse, bodyText);
  // Finish with footer info
  strcat(httpResponse, html_footer);
  // Get the string length
  httpResponseLength = strlen(httpResponse);

  http_send_body(h, httpResponse, httpResponseLength);
}

/* starts a listeninng socket for HTTP connections. */
void initialize_http_service(void)
{
  /* listen for inncoming connections */
  if (http_listen(fio_cli_get("-p"), fio_cli_get("-b"),
                  .on_request = on_http_request,
                  .max_body_size = fio_cli_get_i("-maxbd") * 1024 * 1024,
                  .ws_max_msg_size = fio_cli_get_i("-max-msg") * 1024,
                  .public_folder = fio_cli_get("-public"),
                  .log = fio_cli_get_bool("-log"),
                  .timeout = fio_cli_get_i("-keep-alive"),
                  .ws_timeout = fio_cli_get_i("-ping")) == -1)
  {
    /* listen failed ?*/
    perror("ERROR: facil couldn't initialize HTTP service (already running?)");
    exit(1);
  }
}
