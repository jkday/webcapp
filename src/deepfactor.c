
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <curl/curl.h>

int getSum(int n)
{
    int sum = 0;

    // Note that this loop runs till square root
    // of n
    for (int i = 1; i <= sqrt(n); i++)
    {
        if (n % i == 0)
        {
            // If divisors are equal,take only one
            // of them
            if (n / i == i)
                sum = sum + i;

            else // Otherwise take both
            {
                sum = sum + i;
                sum = sum + (n / i);
            }
        }
    }

    // calculate sum of all proper divisors only
    sum = sum - n;
    return sum;
}

bool checkAbundant(int n)
{
    return (getSum(n) > n);
}

int getPage(void)
{
    CURL *curl;
    CURLcode res;

    curl = curl_easy_init();
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, "http://202.165.23.45/");
        /* example.com is redirected, so we tell libcurl to follow redirection */
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        /* get us the resource without a body - use HEAD! */
        curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
        /* Perform the request, res will get the return code */
        res = curl_easy_perform(curl);
        /* Check for errors */
        if (res != CURLE_OK)
            fprintf(stderr, "curl_easy_perform() failed: %s\n",
                    curl_easy_strerror(res));

        /* always cleanup */
        curl_easy_cleanup(curl);
    }
    return 0;
}
