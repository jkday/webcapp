#!/bin/sh

DISTRO=$(cat /etc/os-release | grep "^ID=" | awk -F = '{print $2}')
OSSTR=$(echo "${DISTRO}" | sed -e 's/^"//' -e 's/"$//')

if [ `id -u` -ne 0 ] ; then
    sudo="sudo"
	if [ "${OSSTR}" = "alpine" ] ; then
		echo "Missing privileges. Kindly re-run with sudo"
		exit 1
	fi
fi

case "${OSSTR}" in
 "centos"|"fedora"|"amzn"|"rhel" )
	${sudo} yum -y install curl
	if [ ! -f /etc/yum.repos.d/deepfactor.repo ] ; then
		cd /etc/yum.repos.d && ${sudo} curl -LO https://repo.deepfactor.io/repo/yum/deepfactor.repo
	fi
	${sudo} rpm --import https://repo.deepfactor.io/repo/yum/keys/D5DC8225.asc
	${sudo} yum -y install deepfactor-runtime
 ;;
 "ubuntu"|"debian"|"linuxmint" )
	${sudo} apt update ; ${sudo} apt install -y curl gnupg
	${sudo} echo "deb https://repo.deepfactor.io/repo/deb/ ./" | tee /etc/apt/sources.list.d/deepfactor.list
	${sudo} curl https://repo.deepfactor.io/repo/keys/D5DC8225.asc | apt-key add -
	${sudo} apt update ; ${sudo} apt install -y deepfactor-runtime
 ;;
 "alpine" )
	if [ ! -f /etc/apk/keys/dfbuild@deepfactor.io-5f35ef3a.rsa.pub ] ; then
		cd /etc/apk/keys && wget https://repo.deepfactor.io/repo/alpine/keys/dfbuild@deepfactor.io-5f35ef3a.rsa.pub
		echo "https://repo.deepfactor.io/repo/alpine" >> /etc/apk/repositories
	fi
	apk add deepfactor-runtime
 ;;
 * )
	echo "Distro ${DISTRO} not supported"
	exit 1
 ;;
esac

