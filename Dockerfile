# Install dfctl and the binary into the Ubuntu container
# This image is from the original release of Ubuntu 18.04
# on April 26,2018. This is done with the intention of generating dependancy
# errors that are found in the runtime observability. 
FROM ubuntu:bionic-20180426 as target
# Download older packages by using sources.list with updated commented out
COPY sources.list /etc/apt/sources.list
# Copy the code into container
COPY . /home/user/webcapp
# Install all packages needed to build, edit code and upload binary to DFP
RUN apt-get update && apt-get install -y make gcc openssh-client vim libcurl4-openssl-dev
WORKDIR /home/user/webcapp
# Install the latest version of dfctl package inside the container to act
# like a regular OS
#RUN  ./install-dfctl.sh
# Build the code
RUN make clean && make
# Inserted for alert
#ENV DEV_API_KEY="key to unlock developer port"
# Inserted for alert
ENV DEBUG="INFO"
# Create entrypoint to run the app or go into a shell if asked.
ENTRYPOINT [ "./docker-entrypoint.sh" ]