#!/bin/bash
set -e

if [ -n "$1" ]; then
    exec "$@"
fi

if [ -z "$PORT" ]; then
    exec ./tmp/fioapp
else
    exec ./tmp/fioapp -p "$PORT"
fi